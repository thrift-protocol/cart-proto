#!/usr/bin/env bash

rm -rf ./gen-*
rm -rf ./src/main/*
rm -rf ./php-src/*
rm -rf ./cartproto

thrift -r --gen php:server yumall.thrift.cart.thrift && \
mv gen-php/* php-src/

thrift -r --gen java yumall.thrift.cart.thrift && \
mv gen-java/* src/main/java

thrift -r --gen go yumall.thrift.cart.thrift && \
mv gen-go/* ./

rm -rf ./gen-*