namespace go cartproto
namespace java com.yumall.thrift.cart
namespace php Yumall.Thrift.Cart

const string SERVER_NAME = "yu-cart-server";

struct Item {
    1: optional string userId;
    2: optional string sku;
    3: optional string count;
    4: optional string status;
}

service Cart {
    list<Item> add(1: Item item);
    list<Item> addMany(1:list<Item> items);
    list<Item> remove(1:list<Item> items);
    list<Item> update(1: Item item);
    list<Item> lists(1: string userId);
}
